package me.creeperkila21.customenchantments.listeners;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;

public class PlayerAttack implements Listener {

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {

		if (e.getDamager() instanceof Player) {
			Player player = (Player) e.getDamager();
			for (CustomEnchant ce : CustomEnchant.allEnchants) {
				ce.onEntityAttack(player, e, player.getItemInHand());
			}
		}else if(e.getDamager() instanceof Player){
			Arrow a = (Arrow) e.getDamager();
			Player player = (Player) a.getShooter();
			for (CustomEnchant ce : CustomEnchant.allEnchants) {
				ce.onShoot(player, e, player.getItemInHand());
			}
		}

	}

	@EventHandler
	public void onShoot(EntityShootBowEvent e){
		
		if(!(e.getEntity() instanceof Player)) return;
		
		Player player = (Player) e.getEntity();
		for (CustomEnchant ce : CustomEnchant.allEnchants) {
			ce.onShoot(player, e, player.getItemInHand());
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		Player player = (Player) e.getPlayer();
		for (CustomEnchant ce : CustomEnchant.allEnchants) {
			ce.onBlockBreak(player, e, player.getItemInHand());
		}
	}
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent e) {
		if(e.getEntity() instanceof Arrow){
			if(e.getEntity().hasMetadata("explosionbow")){
				e.getEntity().getWorld().createExplosion(e.getEntity().getLocation().getX(), e.getEntity().getLocation().getY(), e.getEntity().getLocation().getZ(), 2.0f, false, false);
			}
		}
	}

}
