package me.creeperkila21.customenchantments.listeners;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryEvents implements Listener{

	@EventHandler
	public void onInventoryEvent(InventoryClickEvent e){
		for(ItemStack item : e.getWhoClicked().getInventory().getContents()){
			if(item == null) continue;
			for(CustomEnchant ce : CustomEnchant.allEnchants){
				if(ce == null) continue;
				if(item.containsEnchantment(ce)){
					ce.updateLore(item);
				}
			}
		}
	}
	
}
