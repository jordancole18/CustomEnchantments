package me.creeperkila21.customenchantments.commands;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ceCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(args.length == 0){
			sender.sendMessage(ChatColor.RED + "/ce <Enchantment Name> <Level>");
			return true;
		}
		
		Player player = (Player) sender;
		
		String name = args[0];
		int level = Integer.parseInt(args[1]);
		
		CustomEnchant enchant = CustomEnchant.getEnchantByName(name);
		
		if(enchant == null){
			sender.sendMessage(ChatColor.RED + "That is an invalid enchantment!");
			return true;
		}
		
		CustomEnchant ce = (CustomEnchant) enchant;
		ce.addTo(player.getItemInHand(), level);
		
		player.sendMessage(ChatColor.GREEN + "Enchantment has been added!");
		return true;
	}

}
