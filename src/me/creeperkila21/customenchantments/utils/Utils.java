package me.creeperkila21.customenchantments.utils;

import java.util.Random;

public class Utils {

	public static boolean checkPercentage(int percentGiven) {
		Random rand = new Random();
		int r = rand.nextInt(100);
		if (r < percentGiven)
			return true;
		else
			return false;
	}

	public static boolean checkPercentage(double percentGiven) {
		Random rand = new Random();
		int r = rand.nextInt(100);
		if (r < percentGiven)
			return true;
		else
			return false;
	}
	
}
