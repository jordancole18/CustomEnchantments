package me.creeperkila21.customenchantments.enchants;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import me.creeperkila21.customenchantments.CustomEnchantments;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class CustomEnchant extends EnchantmentWrapper implements
		Listener {

	public static List<CustomEnchant> allEnchants = new ArrayList<CustomEnchant>();

	public static int concurrentID = 120;

	private int id;
	private String name;
	private int startingLevel;
	private int maxLevel;
	private List<Material> enchantables = new ArrayList<Material>();
	private EnchantmentTarget target;

	public CustomEnchant(String name, EnchantmentTarget target) {
		super(concurrentID);
		this.name = name;
		this.startingLevel = 1;
		this.maxLevel = 3;
		setId(concurrentID);
		concurrentID++;
		allEnchants.add(this);
		this.target = target;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getStartLevel() {
		return startingLevel;
	}

	public void setStartLevel(int startingLevel) {
		this.startingLevel = startingLevel;
	}

	@Override
	public int getMaxLevel() {
		return maxLevel;
	}
	
	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public void register() {
		try {
		    Field f = Enchantment.class.getDeclaredField("acceptingNew");
		    f.setAccessible(true);
		    f.set(null, true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		try {
			EnchantmentWrapper.registerEnchantment(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		Bukkit.getPluginManager().registerEvents(this,
				CustomEnchantments.getInstance());
	}

	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return target;
	}

	public void addMaterial(Material... mats) {
		for (Material m : mats) {
			enchantables.add(m);
		}
	}

	public ItemStack addTo(ItemStack item, int level){
		item.addUnsafeEnchantment(this, level);
		return updateLore(item);
	}
	
	public int getCurrentLevel(ItemStack item){
		return item.getEnchantmentLevel(this);
	}
	
	public ItemStack updateLore(ItemStack item){
		if(item.containsEnchantment(this) == false) return null;
		if(containsLore(item)) return null;
		ItemMeta im = item.getItemMeta();
		List<String> lore = new ArrayList<String>();
		if(item.hasItemMeta() == false) return null;
		
		lore.add(getLore());
		lore.add("");
		
		if(item.getItemMeta().getLore() != null){
			for(String k : item.getItemMeta().getLore()){
				lore.add(k);
			}
		}
		
		im.setLore(lore);
		item.setItemMeta(im);
		return item;
	}
	
	public String getLore(){
		return ChatColor.GRAY + getName();
	}
	
	public boolean containsLore(ItemStack item){
		if(item.hasItemMeta() == false) return false;
		if(item.getItemMeta().hasLore() == false) return false;
		if(item.getItemMeta().getLore() == null) return false;
		if(item.getItemMeta().getLore().contains(getLore()) == false) return false;
		return true;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item) {
		if (enchantables.contains(item.getType()))
			return true;
		return false;
	}

	public static CustomEnchant getEnchantByName(String name){
		for(CustomEnchant ce : allEnchants){
			if(ce.getName().equalsIgnoreCase(name)) return ce;
		}
		return null;
	}
	
	public void onEntityAttack(Player player, EntityDamageByEntityEvent e, ItemStack item) {
		
	}
	
	public void onShoot(Player player, EntityDamageByEntityEvent e, ItemStack item){
		
	}

	public void onShoot(Player player, EntityShootBowEvent e, ItemStack item) {
		
	}

	public void onBlockBreak(Player player, BlockBreakEvent e,
			ItemStack itemInHand) {
	}
	
	public void update(Player player){
		
	}
	
}
