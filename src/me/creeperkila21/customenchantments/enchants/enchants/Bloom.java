package me.creeperkila21.customenchantments.enchants.enchants;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;

public class Bloom extends CustomEnchant{

	public Bloom() {
		super("Bloom", EnchantmentTarget.ARMOR);
		this.setMaxLevel(1);
	}

	@Override
	public void update(Player player){
		
		double health = 20;
		
		if(player.getInventory().getHelmet() != null){
			if(player.getInventory().getHelmet().containsEnchantment(this)){
				health+=2;
			}
		}
		if(player.getInventory().getChestplate() != null){
			if(player.getInventory().getChestplate().containsEnchantment(this)){
				health+=6;
			}
		}
		if(player.getInventory().getLeggings() != null){
			if(player.getInventory().getLeggings().containsEnchantment(this)){
				health+=4;
			}
		}
		if(player.getInventory().getBoots() != null){
			if(player.getInventory().getBoots().containsEnchantment(this)){
				health+=2;
			}
		}
		
		player.setMaxHealth(health);
	}
	
}
