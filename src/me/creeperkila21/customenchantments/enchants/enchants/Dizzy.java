package me.creeperkila21.customenchantments.enchants.enchants;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.utils.Utils;

/**
 * Jordan Cole
 * Jan 9, 2018
 * Period 2
 */

public class Dizzy extends CustomEnchant{

	public Dizzy() {
		super("Dizzy", EnchantmentTarget.WEAPON);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onEntityAttack(Player player, EntityDamageByEntityEvent e, ItemStack item) {
		if(!(e.getEntity() instanceof LivingEntity)) return;
		LivingEntity le = (LivingEntity) e.getEntity();		
		if(Utils.checkPercentage(3 * getCurrentLevel(item))){
			PotionEffect pe = new PotionEffect(PotionEffectType.CONFUSION, 3 * 20, 1);
			le.addPotionEffect(pe);
		}
	}
	
}
