package me.creeperkila21.customenchantments.enchants.enchants;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class Burst extends CustomEnchant{

	public Burst() {
		super("Burst", EnchantmentTarget.TOOL);
		this.setMaxLevel(1);
	}

	@Override
	public void onBlockBreak(Player player, BlockBreakEvent e,
			ItemStack itemInHand) {
		
		Block block = e.getBlock();
		Block block2 = e.getBlock().getWorld().getBlockAt(block.getX(), block.getY() + 1, block.getZ());
		Block block3 = e.getBlock().getWorld().getBlockAt(block.getX(), block.getY() - 1, block.getZ());
		
		for(BlockFace bf : BlockFace.values()){
			block.getRelative(bf).breakNaturally();
			block2.getRelative(bf).breakNaturally();
			block3.getRelative(bf).breakNaturally();
		}
		
	}
	
}
