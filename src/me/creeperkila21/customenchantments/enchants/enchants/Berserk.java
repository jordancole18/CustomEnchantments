package me.creeperkila21.customenchantments.enchants.enchants;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.utils.Head.Mob;
import me.creeperkila21.customenchantments.utils.Utils;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class Berserk extends CustomEnchant{

	public Berserk() {
		super("Berserk", EnchantmentTarget.WEAPON);
		this.setMaxLevel(3);
	}

	@Override
	public void onEntityAttack(Player player, EntityDamageByEntityEvent e, ItemStack item) {
		
		if(e.getEntity() instanceof LivingEntity == false) return;
		
		LivingEntity le = (LivingEntity) e.getEntity();
		
		if(le.getHealth() - e.getDamage() > 0){
			return;
		}
		
		if(e.getEntityType() != EntityType.PLAYER){
			if(Utils.checkPercentage(3 * this.getCurrentLevel(item))){
				e.getEntity().getLocation().getWorld().dropItem(e.getEntity().getLocation(), Mob.getFromType(e.getEntityType()).getHead());
			}
		}
		
	}
	
	

}
