package me.creeperkila21.customenchantments.enchants.enchants;

import me.creeperkila21.customenchantments.CustomEnchantments;
import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.utils.Utils;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

public class Bazooka extends CustomEnchant {

	public Bazooka() {
		super("Bazooka", EnchantmentTarget.BOW);
		this.setMaxLevel(3);
	}

	@Override
	public void onShoot(Player player, EntityShootBowEvent e, ItemStack item) {

		if(Utils.checkPercentage(7 * getCurrentLevel(item))){
			Arrow a = (Arrow) e.getProjectile();
			setMetadata(a, "explosionbow", "", CustomEnchantments.getInstance());
		}
		
	}

	private void setMetadata(Projectile prj, String key, Object value,
			Plugin plugin) {
		prj.setMetadata(key, new FixedMetadataValue(plugin, value));
	}

}
