package me.creeperkila21.customenchantments.enchants.enchants;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.utils.Head;
import me.creeperkila21.customenchantments.utils.Utils;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

public class BountyHunter extends CustomEnchant {

	public BountyHunter() {
		super("Bounty Hunter", EnchantmentTarget.WEAPON);
		this.setMaxLevel(3);
	}

	@Override
	public void onEntityAttack(Player player, EntityDamageByEntityEvent e,
			ItemStack item) {

		if(!(e.getEntity() instanceof Player)) return;

		Player p = (Player) e.getEntity();
		
		LivingEntity le = (LivingEntity) e.getEntity();

		if (le.getHealth() - e.getDamage() > 0) {
			return;
		}

		if (e.getEntityType() == EntityType.PLAYER) {
			if (Utils.checkPercentage(15 * this.getCurrentLevel(item))) {
				e.getEntity()
						.getLocation()
						.getWorld()
						.dropItem(e.getEntity().getLocation(),
								new Head(p.getName(), p.getName()).getHead());
			}
		}

	}

}
