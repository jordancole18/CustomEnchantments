package me.creeperkila21.customenchantments.enchants.enchants;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.utils.Utils;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Blizzard extends CustomEnchant{

	public Blizzard() {
		super("Bliazzard", EnchantmentTarget.ALL);
		this.setMaxLevel(2);
	}

	@Override
	public void onEntityAttack(Player player, EntityDamageByEntityEvent e, ItemStack item) {
		if(!(e.getEntity() instanceof LivingEntity)) return;
		LivingEntity le = (LivingEntity) e.getEntity();
		if(Utils.checkPercentage(1.5 * getCurrentLevel(item))){
			PotionEffect pe = new PotionEffect(PotionEffectType.SLOW, (3 + getCurrentLevel(item)) * 20, 1);
			le.addPotionEffect(pe);
		}
	}
	
}
