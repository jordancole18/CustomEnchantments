package me.creeperkila21.customenchantments.enchants.enchants;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.utils.Utils;

/**
 * Jordan Cole Jan 9, 2018 Period 2
 */

public class DoubleShot extends CustomEnchant {

	public DoubleShot() {
		super("Double Shot", EnchantmentTarget.BOW);
		setMaxLevel(1);
	}

	@Override
	public void onShoot(Player player, EntityDamageByEntityEvent e, ItemStack item){
		
		if(e.getDamager() instanceof Arrow){
			if(Utils.checkPercentage(5)){
				e.setDamage(e.getDamage() * 2);
			}
		}
		
	}
	
}
