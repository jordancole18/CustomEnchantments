package me.creeperkila21.customenchantments.enchants.enchants;

import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.utils.Utils;

/**
 * Jordan Cole Jan 9, 2018 Period 2
 */

public class Combo extends CustomEnchant {

	public Combo() {
		super("Combo", EnchantmentTarget.WEAPON);
		this.setMaxLevel(3);
	}

	@Override
	public void onEntityAttack(Player player, EntityDamageByEntityEvent e, ItemStack item) {
		
		if(Utils.checkPercentage(5 * getCurrentLevel(item))){
			double percent = (5 * getCurrentLevel(item) * 0.01);
			double extraDamage = e.getDamage() * percent;
			e.setDamage(e.getDamage() + extraDamage);
		}
		
	}

}
