package me.creeperkila21.customenchantments.enchants.enchants;

import org.bukkit.enchantments.EnchantmentTarget;

import me.creeperkila21.customenchantments.enchants.CustomEnchant;

/**
 * Jordan Cole Jan 9, 2018 Period 2
 */

public class Cure extends CustomEnchant {

	public Cure() {
		super("Cure", EnchantmentTarget.ARMOR);
		this.setMaxLevel(1);
	}
	
}
