package me.creeperkila21.customenchantments;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.creeperkila21.customenchantments.commands.ceCommand;
import me.creeperkila21.customenchantments.enchants.CustomEnchant;
import me.creeperkila21.customenchantments.enchants.enchants.Bazooka;
import me.creeperkila21.customenchantments.enchants.enchants.Berserk;
import me.creeperkila21.customenchantments.enchants.enchants.Blackout;
import me.creeperkila21.customenchantments.enchants.enchants.Bloom;
import me.creeperkila21.customenchantments.enchants.enchants.BountyHunter;
import me.creeperkila21.customenchantments.enchants.enchants.Burst;
import me.creeperkila21.customenchantments.enchants.enchants.Combo;
import me.creeperkila21.customenchantments.enchants.enchants.ConcussiveBlow;
import me.creeperkila21.customenchantments.enchants.enchants.Dizzy;
import me.creeperkila21.customenchantments.enchants.enchants.DoubleShot;
import me.creeperkila21.customenchantments.listeners.InventoryEvents;
import me.creeperkila21.customenchantments.listeners.PlayerAttack;

public class CustomEnchantments extends JavaPlugin{

	private static CustomEnchantments instance;
	
	@SuppressWarnings("deprecation")
	public void onEnable(){
		Bukkit.getPluginManager().registerEvents(new PlayerAttack(), this);
		Bukkit.getPluginManager().registerEvents(new InventoryEvents(), this);
		instance = this;
		registerEnchantments();
		getCommand("ce").setExecutor(new ceCommand());
		
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, new Runnable(){
			@Override
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()){
					
					for(CustomEnchant ce : CustomEnchant.allEnchants){
						ce.update(p);
					}
					
				}
			}
		}, 10, 10);
		
	}
	
	public static CustomEnchantments getInstance(){
		return instance;
	}

	public void registerEnchantments(){
		new Berserk().register();
		new BountyHunter().register();
		new Bazooka().register();
		new Burst().register();
		new Blackout().register();
		new Bloom().register();
		/*
		 * TODO
		 * FINISH THE
		 * CURE Enchantment
		 * DETONATE Enchantment
		 */
		new Combo().register();
		new ConcussiveBlow().register();
		new Dizzy().register();
		new DoubleShot().register();
	}
	
}
